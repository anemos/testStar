import {
    ADD_PART,
    REMOVE_PART,
    SET_START,
    UNSET_START,
} from '../constants';

export const addPart = ( value, key ) => ({ 
    type: ADD_PART, 
    payload: {
        id: key,
        titles: value,
    }
});
export const removePart = ( key )  => ({ type: REMOVE_PART, payload: key })
export const setStart = () => ({type: SET_START, payload: true})
export const unsetStart = () => ({type: UNSET_START, payload: false})