import { createStore, compose, applyMiddleware } from 'redux';
import thunk  from 'redux-thunk';
import reducer from '../reducers/reducers';

const middlewares = applyMiddleware(
    thunk
);

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store = createStore( reducer, composeEnhancers( middlewares ) );


export default store;