import {
    ADD_PART,
    REMOVE_PART,
    SET_START,
    UNSET_START,
} from '../constants';

const initState = {
    start: false,
    data: []
}

const reducer = ( state = initState, action ) => {
    switch( action.type ){
        case ADD_PART:
            return({
                ...state,
                data: [ ...state.data, action.payload ]
            });

        case REMOVE_PART:
            return({
                ...state,
                data: state.data.filter( item => item.id !== action.payload )
            });

        case SET_START:
            return({
                ...state,
                start: action.payload
            });

        case UNSET_START:
            return({
                data: [],
                start: action.payload
            });

        default:
            return state;

    }
}

export default reducer;