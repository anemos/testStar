import React from 'react'
import { useSelector } from 'react-redux';
import { getStateSelector } from '../selector';
import PropTypes from 'prop-types';

import Col from '../component/table/Col';

const HookStatus = ({cordinats, id}) => {
    const { start } = useSelector(getStateSelector);

    return(
        <Col starters={start} cordinats={cordinats} id={id}/>
    )
}

HookStatus.propTypes = {
    cordinats: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
}

export default HookStatus;
