import React from 'react'
import { useSelector } from 'react-redux';
import { getStateSelector } from '../selector';

import ListHover from '../component/list/ListHover';

const HookList = () => {
    const { data } = useSelector(getStateSelector);

    return(
        <ListHover data={data} />
    )
}

export default HookList;
