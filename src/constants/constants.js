export const ADD_PART = 'ADD_PART';
export const REMOVE_PART = 'REMOVE_PART';
export const SET_START = 'SET_START';
export const UNSET_START = 'UNSET_START';