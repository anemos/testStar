import React from 'react';
import { Provider } from 'react-redux';

import store from '../redux/store';

import TableWrap from '../component/table/TableWrap';
import HookList from '../container/HookList';

const Wrapers = () => {

    return(
        <Provider store={store}>
            <h1>StarNavi: Тестовое задание</h1>
            <div className="grid">
                <div className="box">
                    <TableWrap/>
                    <p className="info">*Pick mode and press start</p>
                </div>
                <div className="box">
                    <h2>Hover squares</h2>
                    <HookList />
                </div>
            </div>
        </Provider>
    );
}

export default Wrapers;