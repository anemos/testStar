import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { addPart, removePart } from '../../actions';

const Col = React.memo(({ starters, cordinats, id }) => {
    const [marker, setMarker] = useState(false);
    const dispatch = useDispatch();

    const changer = () => {
        setMarker(!marker);
        if(!marker){
            dispatch(addPart(cordinats, id))
        }else{
            dispatch(removePart(id))
        }
    }

    return(
        <>
            {
                starters
                ? <td onMouseOver={changer} height="50px" width="50px" className={marker ? 'colored' : 'default-color'}></td>
                : <td height="50px" width="50px"></td>
            }
        </>
    );
})

Col.propTypes = {
    starters: PropTypes.bool.isRequired,
    cordinats: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
}

export default Col;