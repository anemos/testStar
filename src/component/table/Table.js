import React from 'react';
import PropTypes from 'prop-types';

import Row from './Row';

const Table = ({ counter }) => {
    const style = {
        maxWidth: `${50 * counter}px`
    }

    return(
        <div style={style} className="table__wrap">
            <table>
                <tbody>
                    <Row count={ counter }/>
                </tbody>
            </table>
        </div>
    );
}

Table.propTypes = {
    counter: PropTypes.number.isRequired,
}

export default Table;