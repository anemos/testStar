import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import uniqid from 'uniqid';

import { setStart, unsetStart } from '../../actions';
import Table from './Table'

const TableWrap = () => {
    const [data, setData] = useState();
    const [list, setList] = useState(
        {
            name: 'Pick mode',
            count: 0,
        }
    );
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                `https://demo1030918.mockable.io/`,
            );
            var list = [];
            list.push({name: 'Pick mode', counter: 0})
            for(var key in result.data){
                list.push(
                    {
                        name: key,
                        counter: result.data[key].field
                    }
                )
            };
            setData(list);
        };
        fetchData();
    }, []);

    const pickMode = (e) => {
        data.forEach(item => {
            if(item.name === e.target.value){
                setList(
                    {
                        name: e.target.value,
                        count: item.counter
                    }
                )
            }
        });
        dispatch( unsetStart() )
    }

    const start = () => {
        dispatch( setStart() )
    }

    if(data === undefined){
        return <div>Loading...</div>
    };

    return(
        <>
            <select onChange={pickMode} value={list.name} className="select__field">
                {
                    data.map(item => {
                        return <option key={uniqid('item-')} value={item.name}>{item.name}</option>
                    })
                }
                
            </select>
            <button onClick={start}>START</button>
            {
                list.count !== 0
                ? <Table counter={list.count}/>
                : ''
            }
        </>
    );
}

export default TableWrap;