import React from 'react';
import PropTypes from 'prop-types';

import HookStatus from '../../container/HookStatus';

import uniqid from 'uniqid';

const Row = ({ count }) => {
    const createRow = () => {
        let row = []
        for (let i = 0; i < count; i++) {
            let children = []
            for (let j = 0; j < count; j++) {
                let keyId = uniqid('item-');
                children.push(<HookStatus cordinats={`row ${i+1} col ${j+1}`} key={keyId} id={keyId}/>)
            }
            row.push(<tr key={uniqid('item-')}>{children}</tr>)
        }
        return row
    }

    return(
        <>
            {
                createRow()
            }
        </>
    );
}

Row.propTypes = {
    count: PropTypes.number.isRequired,
}

export default Row;