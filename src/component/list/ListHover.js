import React from 'react';
import PropTypes from 'prop-types';

import Line from './Line';

const ListHover = ({ data }) => {
    return(
        <ul className="lists">
            {
                data.map((item, index) => {
                    return <Line key={item.id + index} titles={item.titles}/>
                })
            }
        </ul>
    );
}

ListHover.propTypes = {
    data: PropTypes.array,
}

export default ListHover;