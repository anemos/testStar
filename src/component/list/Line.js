import React from 'react';
import PropTypes from 'prop-types';

const Line = React.memo(({ titles }) => {
    return(
        <li>{titles}</li>
    );
})

Line.propTypes = {
    titles: PropTypes.string.isRequired,
}

export default Line;